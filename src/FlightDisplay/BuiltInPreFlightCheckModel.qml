/****************************************************************************
 *
 *   (c) 2009-2016 QGROUNDCONTROL PROJECT <http://www.qgroundcontrol.org>
 *
 * QGroundControl is licensed according to the terms in the file
 * COPYING.md in the root of the source code directory.
 *
 ****************************************************************************/

import QtQuick                      2.3
import QtQml.Models                 2.1

import QGroundControl               1.0
import QGroundControl.FlightDisplay 1.0
import QGroundControl.ScreenTools   1.0
import QGroundControl.Controls      1.0
import QGroundControl.Palette       1.0
import QGroundControl.Vehicle       1.0

PreFlightCheckModel {
    PreFlightCheckGroup {
        name: qsTr("Initial checks")

        // Standard check list items (group 0) - Available from the start
        PreFlightCheckButton {
            name:           qsTr("Hardware")
            manualText:     qsTr("Frame in good physical condition")
        }

        PreFlightCheckButton {
            name:           qsTr("Batteries")
            manualText:     qsTr("Batteries in good physical condition")
        }

        PreFlightBatteryCheck {
            failurePercent:                 0
            failureVolts:                   29.8
            allowFailurePercentOverride:    false
        }

        PreFlightCheckButton {
            name:           qsTr("Bird Diverters Loaded")
            manualText:     qsTr("Bird Diverters are Loaded")
        }
        
        PreFlightCheckButton {
            name:           qsTr("Drums Clear")
            manualText:     qsTr("Drums Clear of Obstructions")
        }

        PreFlightCheckButton {
            name:           qsTr("Install Dist")
            manualText:     qsTr("Install Distance is Correct")
        }

        PreFlightCheckButton {
            name:           qsTr("HereLink Battery")
            manualText:     qsTr("Above 30% or charging")
        }

        PreFlightSensorsHealthCheck {
        }

        PreFlightCheckButton {
            name:           qsTr("Video")
            manualText:     qsTr("Good Video on Screen")
        }

        PreFlightCheckButton {
            name:           qsTr("Drive Wheels")
            manualText:     qsTr("Drive Wheels Respond to Driving Command")
        }

        // PreFlightGPSCheck {
        //     failureSatCount:        9
        //     allowOverrideSatCount:  true
        // }

        PreFlightRCCheck {
        }

        PreFlightSoundCheck {
        }
    }

    PreFlightCheckGroup {
        name: qsTr("Please arm the vehicle here")

        PreFlightCheckButton {
            name:            qsTr("Servos")
            manualText:      qsTr("Did the install servos move as expected?")
        }

        PreFlightCheckButton {
            name:            qsTr("Motors")
            manualText:      qsTr("Propellers free? Then throttle up gently. Working properly?")
        }

    }

    PreFlightCheckGroup {
        name: qsTr("Last preparations before launch")

        // Check list item group 2 - Final checks before launch
        PreFlightCheckButton {
            name:        qsTr("Payload")
            manualText:  qsTr("Configured and started? Payload lid closed?")
        }

        PreFlightCheckButton {
            name:        "Wind & weather"
            manualText:  qsTr("OK for your platform? Lauching into the wind?")
        }

        PreFlightCheckButton {
            name:        qsTr("Flight area")
            manualText:  qsTr("Launch area and path free of obstacles/people?")
        }
    }
} // Object Model
