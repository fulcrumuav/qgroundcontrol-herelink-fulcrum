/****************************************************************************
 *
 *   (c) 2009-2016 QGROUNDCONTROL PROJECT <http://www.qgroundcontrol.org>
 *
 * QGroundControl is licensed according to the terms in the file
 * COPYING.md in the root of the source code directory.
 *
 ****************************************************************************/

import QtQuick                  2.3
import QtQuick.Controls         1.2

import QGroundControl               1.0
import QGroundControl.Controls      1.0
import QGroundControl.Vehicle       1.0

Rectangle {
    id:                 _root

    property var  _flyViewSettings:     QGroundControl.settingsManager.flyViewSettings
    property var  _activeVehicle:       QGroundControl.multiVehicleManager.activeVehicle
    property bool _fixedWing:           false

    property real max: _activeVehicle ? _activeVehicle.getNumInstalls() : 24
    property real min: 1

    function reset() {
        posSlider.value = 1
    }

    function setToCurrent(value) {
        posField.setToCurrent(value)
        max = _activeVehicle ? _activeVehicle.getNumInstalls() : 24
    }

    function setTitle(value) {
        titleField.text = value;
    }
    function getValue() {
        return posField.newPos
    }

    function log10(value) {
        if (value === 0) {
            return 0
        } else {
            return Math.log(value) / Math.LN10
        }
    }

    Column {
        id:                 headerColumn
        anchors.margins:    _margins
        anchors.top:        parent.top
        anchors.left:       parent.left
        anchors.right:      parent.right

        QGCLabel {
            id:                     titleField
            anchors.left:           parent.left
            anchors.right:          parent.right
            wrapMode:               Text.WordWrap
            horizontalAlignment:    Text.AlignHCenter
            text:                   qsTr("Set Pos")
        }

        QGCLabel {
            id:                         posField
            anchors.horizontalCenter:   parent.horizontalCenter
            text:                       newPosStr

            property real   newPos:     posSlider.value.toFixed(0)
            property string newPosStr:  newPos.toString()

            function setToCurrent(value) {
                if (value > 0){
                    posSlider.value = value
                } else {
                    posSlider.value = 1
                }
            }
        }
    }

    QGCSlider {
        id:                 posSlider
        anchors.margins:    _margins
        anchors.top:        headerColumn.bottom
        anchors.bottom:     parent.bottom
        anchors.left:       parent.left
        anchors.right:      parent.right
        orientation:        Qt.Vertical
        minimumValue:       parent.min
        maximumValue:       parent.max
        zeroCentered:       true
        rotation:           180

        // We want slide up to be positive values
        transform: Rotation {
            origin.x:   posSlider.width / 2
            origin.y:   posSlider.height / 2
            angle:      180
        }
    }
}
