/****************************************************************************
 *
 *   (c) 2009-2016 QGROUNDCONTROL PROJECT <http://www.qgroundcontrol.org>
 *
 * QGroundControl is licensed according to the terms in the file
 * COPYING.md in the root of the source code directory.
 *
 ****************************************************************************/

import QtQuick 2.3

import QGroundControl           1.0
import QGroundControl.Controls  1.0
import QGroundControl.Vehicle   1.0

// This class stores the data and functions of the check list but NOT the GUI (which is handled somewhere else).
PreFlightCheckButton {
    name:                           qsTr("Battery")
    manualText:                     failureVolts > 0 ? qsTr("Battery over %1V and connector firmly plugged?").arg(failureVolts) :
                                        qsTr("Battery connector firmly plugged?")
    telemetryFailure:               _batLow
    telemetryTextFailure:           allowTelemetryFailureOverride ?
                                        qsTr("Warning - Battery charge below %1V.").arg(failureVolts) :
                                        qsTr("Battery charge below %1V. Please recharge.").arg(failureVolts)
    allowTelemetryFailureOverride:  allowFailurePercentOverride

    property int    failurePercent:                 40
    property double failureVolts:                   0
    property bool   allowFailurePercentOverride:    false

    property var _activeVehicle:        QGroundControl.multiVehicleManager.activeVehicle
    property var _batPercentRemaining:  _activeVehicle ? _activeVehicle.battery.percentRemaining.value : 0
    property var _batVRemaining:        _activeVehicle ? _activeVehicle.battery.voltage.value : 0
    property bool _batLow:              failureVolts > 0 ? _batVRemaining < failureVolts: _batPercentRemaining < failurePercent
}
